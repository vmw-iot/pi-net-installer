import os
import shutil
import urllib.request
import gnupg
import gzip

BOOT_URL = 'https://downloads.raspberrypi.org/raspbian/boot.tar.xz'
RASPBIAN_REPO_URL = 'http://mirrordirector.raspbian.org/raspbian'
RASPBIAN_KEY_URL = 'http://mirrordirector.raspbian.org/raspbian.public.key'
RASPBIAN_SECTIONS = ['main', 'firmware', 'non-free']
RPI_DEBIAN_REPO_URL = 'http://archive.raspberrypi.org/debian'
RPI_DEBIAN_KEY_URL = 'http://archive.raspberrypi.org/debian/raspberrypi.gpg.key'
RPI_DEBIAN_SECTIONS = ['main']
RELEASE = 'buster'

RASPBIAN = 'raspbian'
RPI_DEBIAN = 'debian'

REPO_DIR = './repo'  # local repository dir
RASPBIAN_REPO_DIR = REPO_DIR + '/' + RASPBIAN
RPI_DEBIAN_REPO_DIR = REPO_DIR + '/' + RPI_DEBIAN
GNUPG_DIR = REPO_DIR + '/gnupg'
PACKAGES_DIR = REPO_DIR + '/packages'

GPG = gnupg.GPG(binary='/usr/local/bin/gpg2', homedir=GNUPG_DIR, ignore_homedir_permissions=True)


# import gpg keys to gnupg pubring
def import_gpg_keys():
    raspbian_key = download_file(RASPBIAN_KEY_URL, GNUPG_DIR)
    rpi_debian_key = download_file(RPI_DEBIAN_KEY_URL, GNUPG_DIR)

    keys = [(raspbian_key, 'A0DA38D0D76E8B5D638872819165938D90FDDD2E'),
            (rpi_debian_key, 'CF8A1AF502A2AA2D763BAE7E82B129927FA3303E')]

    for key in keys:
        key_data = open(key[0]).read()
        gpg_import = GPG.import_keys(key_data)
        if gpg_import.fingerprints[0] != key[1]:
            print('Bad fingerprint for %s: %s' % (key[0], key[1]))
        else:
            print('Fingerprint for %s OK!' % key[0])


# download a file from url to destination directory and return downloaded file path
def download_file(from_url, to_dir):
    filename = from_url[from_url.rfind('/') + 1:]
    destination = '%s/%s' % (to_dir, filename)
    urllib.request.urlretrieve(from_url, destination)
    return destination


# download a repository metadata from url and put it to local_repo directory
def download_repo_metadata(url, local_path, sections):
    repo_url = '%s/dists/%s' % (url, RELEASE)

    # download Release file with signature and verify them
    dwn_file = download_file('%s/%s' % (repo_url, 'Release'), local_path)
    gpg_file = download_file('%s/%s' % (repo_url, 'Release.gpg'), local_path)
    # v = GPG.verify_file(dwn_file, gpg_file)

    # download packages info
    for section in sections:
        d = '%s/%s' % (local_path, section)
        os.makedirs(d)
        gz = download_file('%s/%s/binary-armhf/Packages.gz' % (repo_url, section), d)
        f = unpack_gz(gz)


# unpack a gz file, returns unpacked file path
def unpack_gz(a_file):
    out_file = os.path.splitext(a_file)[0]  # os.path.basename(your_path)

    with gzip.open(a_file, 'rb') as f_in:
        with open(out_file, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

    return out_file


# parse packages list
def download_packages():
    # local_repo_dirs = ['%s/%s' % (RASPBIAN_REPO_DIR, s) for s in RASPBIAN_SECTIONS]
    # local_repo_dirs += ['%s/%s' % (RPI_DEBIAN_REPO_DIR, s) for s in RPI_DEBIAN_SECTIONS]
    with open('./packages.list') as f:
        for line in f:
            line = line.strip()
            if line and (not line.startswith('#')):
                print('Package: ' + line)
                package_r = find_deb(line, RASPBIAN)
                package_d = find_deb(line, RPI_DEBIAN)
                if package_r is not None:
                    url = RASPBIAN_REPO_URL + '/' + package_r
                    print('Found in RASPBIAN, downloading from %s...' % url)
                    download_file(url, PACKAGES_DIR)
                elif package_d is not None:
                    url = RPI_DEBIAN_REPO_URL + '/' + package_d
                    print('Found in RPI_DEBIAN, downloading from %s...' % url)
                    download_file(url, PACKAGES_DIR)
                else:
                    print('Could not find package: ' + line)
    download_file('https://downloads.raspberrypi.org/raspbian/boot.tar.xz', PACKAGES_DIR)


def find_deb(package, repo_name):
    local_repo_dir = ''
    sections = []
    if repo_name == RASPBIAN:
        local_repo_dir = RASPBIAN_REPO_DIR
        sections = RASPBIAN_SECTIONS
    elif repo_name == RPI_DEBIAN:
        local_repo_dir = RPI_DEBIAN_REPO_DIR
        sections = RPI_DEBIAN_SECTIONS

    for section in sections:
        packages_file = '%s/%s/Packages' % (local_repo_dir, section)
        with open(packages_file, 'r') as search:
            package_name = ''
            package_deb = ''
            package_md5 = ''
            for line in search:
                line = line.strip()
                if line.startswith('Package:'):
                    package_name = line[line.rfind(':') + 1:]  # from_url[from_url.rfind('/') + 1:]
                elif line.startswith('Filename:'):
                    package_deb = line[line.rfind(':') + 1:]
                elif line.startswith('MD5sum:'):
                    package_md5 = line[line.rfind(':') + 1:]
                elif not line:
                    # print('%s:%s:%s' % (package_name.strip(), package_deb.strip(), package_md5.strip()))
                    if package_name.strip() == package:
                        # return package_name, package_deb, package_md5, repo_name, section
                        return package_deb.strip()
                    package_name = ''
                    package_deb = ''
                    package_md5 = ''
    return None


def main():
    shutil.rmtree(RASPBIAN_REPO_DIR, ignore_errors=True)
    shutil.rmtree(RPI_DEBIAN_REPO_DIR, ignore_errors=True)
    shutil.rmtree(PACKAGES_DIR, ignore_errors=True)
    os.makedirs(RASPBIAN_REPO_DIR)
    os.makedirs(RPI_DEBIAN_REPO_DIR)
    os.makedirs(PACKAGES_DIR)

    import_gpg_keys()

    download_repo_metadata(RASPBIAN_REPO_URL, RASPBIAN_REPO_DIR, RASPBIAN_SECTIONS)
    download_repo_metadata(RPI_DEBIAN_REPO_URL, RPI_DEBIAN_REPO_DIR, RPI_DEBIAN_SECTIONS)

    download_packages()


if __name__ == '__main__':
    main()

# try:
#     os.mkdir(REPO_DIR)
# except OSError:
#     print('Creation of the directory %s failed' % REPO_DIR)
# else:
#     print('Successfully created the directory %s ' % REPO_DIR)
# shutil.rmtree(REPO_DIR)
